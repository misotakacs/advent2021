import sys
import collections

def getlines():
    ip = []
    for line in sys.stdin:
            l = line.strip()
            ip.append(l)

    return ip


class node:
    def __init__(self, name):
        self.name = name
        self.small = name.lower() == name
        # other nodes
        self.ns = []
    def __str__(self):
        return self.name

def search(n, vis, twice):
#    print n, twice
    if n.name == 'end':
        return 1
    if n.name in vis:
        if n.name != 'start' and not twice:
            twice = n.name
        else:
#            print 'already visited'
            return 0
    if n.small:
        vis.add(n.name)
    cnt = sum([search(cn, vis, twice) for cn in n.ns])
    if n.small and n.name != twice:
        vis.remove(n.name)
    return cnt

def run():
    lines = getlines()
    g = {}
    for l in lines:
        gs = l.split('-')
        g1 = gs[0]
        g2 = gs[1]
        gn1 = g.get(g1, node(g1))
        gn2 = g.get(g2, node(g2))
        gn1.ns.append(gn2)
        gn2.ns.append(gn1)
        g[g1] = gn1
        g[g2] = gn2

    print search(g['start'], set(), None)




run()
