import sys
import re
import math
import itertools
from functools import reduce

def init():
    lines = []

    for line in sys.stdin:
        lines.append(line.strip())

    return lines


def run():
    ls = init()

    m = ls[0]

    minx = 0
    miny = 0
    maxx = len(ls[2])
    maxy = len(ls[2:])

    px = {}

    for y, l in enumerate(ls[2:]):
        for x, c in enumerate(l):
            if c == '#':                
                px[(x, y)] = True

    def prn(r):        
        for y in range(miny, maxy):
            s = ''
            for x in range(minx, maxx):
                if getpx(x, y, r):
                  s += '#'
                else:
                  s += '.'
            print(s)

    def getpx(x, y, r):        
        if x < minx or x > maxx or y < miny or y > maxy:
            return r
        else:
            a = px.get((x, y),False)            
            return a

    dp = [(-1, -1), (0, -1), (1, -1), (-1, 0), (0, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]

    def getval(x, y, r):
        n = 0
        for (dx, dy) in dp:            
            a = getpx(x+dx, y+dy, r)
            n = (n << 1) + (1 if a else 0)        
        return m[n] == '#'

    #prn(False)

    r = False
    for it in range(0, 50):
        npx = {}                
        cnt = 0
        for x in range(minx-2, maxx+3):
            for y in range(miny-2, maxy+3):
                res = getval(x, y, r)
                if res:
                    npx[(x, y)] = res
                    cnt += 1
        minx -=2
        miny -=2
        maxx +=2
        maxy +=2
        px = npx

        r = r^(m[0] == '#')
        #prn(r)

        print('Round %d, cnt=%d' % (it, cnt))

run()
