package main

import (
	"bufio"
	"flag"
	"fmt"
	"image"
	"image/color"
	"image/gif"
	"os"
)

var (
	// should be 32x32
	oc = []string{
		"",
		"",
		"                   xxx",
		"                 xxxxxxx",
		"                xxxxxxxxx",
		"               xxxxxxxxxxx",
		"               xxxxxxxxxxx",
		"               xxx  xx  xx",
		"          xxx  xxxxxxxxxxx",
		"         xxxxx  xxxxxxxxx",
		"         xx xx  xxxx  xxx  xxxx",
		"          x xxx  xxxxxxx  xxxxxx",
		"             xxxxxxxxxxx  xx  xx",
		"             xxxxxxxxxxxxxx xx",
		"               xxxxxxxxxxx",
		"              xxxxxx xx",
		"            xxxxx xx xxxx",
		"           xxx    xxx xxxx",
		"           xx      xxx  xxx",
		"            xx      xx   xx",
		"             x      xx   xx",
		"            xx     xxx  xx",
		"                  xxx",
		"                  x",
		"                  xx",
		"",
		"",
	}
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

func frame(m [][]int) *image.Paletted {

	wy := len(m)
	wx := len(m[0])

	h := len(oc)
	w := 0
	for _, l := range oc {
		if w < len(l) {
			w = len(l)
		}
	}

	pal := []color.Color{color.Black}
	for i := 1; i < 20; i++ {
		pal = append(pal, color.RGBA{20, 20, uint8(i * 25), 255})
	}
	//palette := []color.Color{color.White, color.Black, color.RGBA{}}
	rect := image.Rect(0, 0, wx*w, wy*h)
	img := image.NewPaletted(rect, pal)
	for y := 0; y < wy; y++ {
		for x := 0; x < wx; x++ {
			for yy := 0; yy < h; yy++ {
				for xx := 0; xx < w; xx++ {
					c := 0
					if xx < len(oc[yy]) {
						if oc[yy][xx] == 'x' {
							c = m[y][x]
						} else {
							c = 0
						}
					}
					img.SetColorIndex(x*w+xx, y*h+yy, uint8(c))
				}
			}
		}
	}

	return img
}

type pt struct {
	x, y int
}

var (
	d = []pt{{-1, -1}, {0, -1}, {1, -1}, {-1, 0}, {1, 0}, {-1, 1}, {0, 1}, {1, 1}}
)

func step(m [][]int, render bool) ([]*image.Paletted, int) {

	fs := []*image.Paletted{}

	fd := map[pt]bool{}
	f := []pt{}
	// seed the flashers
	for rx, r := range m {
		for cx, c := range r {
			m[rx][cx] = c + 1
			if c > 8 {
				f = append(f, pt{cx, rx})
			}
		}
	}

	if render {
		fs = append(fs, frame(m))
	}

	// flash
	for len(f) > 0 {
		nf := f[0]
		f = f[1:]
		fd[nf] = true
		for _, p := range d {
			nx := nf.x + p.x
			ny := nf.y + p.y
			if nx < 0 || ny < 0 || ny >= len(m) || nx >= len(m[0]) {
				continue
			}
			m[ny][nx] += 1
			if !fd[pt{nx, ny}] && m[ny][nx] == 10 {
				f = append(f, pt{nx, ny})
			}
		}
		if render {
			fs = append(fs, frame(m))
		}
	}

	// reset overflashers
	for rx, r := range m {
		for cx, c := range r {
			if c > 9 {
				m[rx][cx] = 0
			}
		}
	}
	if render {
		fs = append(fs, frame(m))
	}
	return fs, len(fd)
}

func main() {
	s := readLines()

	flag.Parse()

	m := [][]int{}

	for _, l := range s {
		r := []int{}
		for _, c := range l {
			r = append(r, int(c-'0'))
		}
		m = append(m, r)
	}

	fmt.Printf("%v\n", m)

	g := &gif.GIF{
		Delay: []int{},
		Image: []*image.Paletted{},
	}

	sum := 0
	render := false

	for i := 0; i < 1000; i++ {
		frames, cnt := step(m, render)
		if cnt == 100 {
			fmt.Printf("all: %d\n", i+1)
		}
		sum += cnt
		g.Image = append(g.Image, frames...)
	}

	fmt.Printf("flashes: %d\n", sum)

	for _ = range g.Image {
		g.Delay = append(g.Delay, 5)
	}

	if render {
		f, err := os.Create("img.gif")
		defer f.Close()
		if err != nil {
			fmt.Printf("Failed to create a file: %v", err)
			os.Exit(1)
		}

		w := bufio.NewWriter(f)

		gif.EncodeAll(w, g)
		w.Flush()
	}

	fmt.Printf("%v\n", m)
}
