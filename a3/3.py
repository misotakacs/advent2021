import string
import sys

def ml(lines):
	cnts = []
	total = 0

	nums = []

	for line in lines:
		if not cnts:
			cnts = [0] * len(line)
		for ix, c in enumerate(line):
			cnts[ix] += int(c)
		total = total + 1

	return ''.join([str(int(round(float(i)/total))) for i in cnts])

def run():
	lines = []

	total = 0

	nums = []

	for line in sys.stdin:
		lines.append(line.strip())

	w = len(lines[0])

	ms = ml(lines)
	x = 2**w-1
	g = int(ms, 2)
	e = g^x

	print '1:', g*e

	i = 0
	linesO = lines
	linesC = lines

	while len(linesO) + len(linesC) > 2:
		mlO = ml(linesO)
		if len(linesO) > 1:
			linesO = filter(lambda x: x[i] == mlO[i], linesO)
		mlC = ml(linesC)
		if len(linesC) > 1:
			linesC = filter(lambda x: x[i] != mlC[i], linesC)
		i = i + 1

	print '2:',int(linesO[0],2)*int(linesC[0],2)

run()

