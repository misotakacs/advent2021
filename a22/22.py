import sys
import re
import itertools
import collections

def init():
    lines = []

    for line in sys.stdin:
        lines.append(line.strip())

    return lines

Cub = collections.namedtuple('Cub', ['x1', 'x2', 'y1', 'y2', 'z1', 'z2', 'on'])

def is1(x1, x2, w1, w2):
    if x1 >= w1 and x2 <= w2:
        return (x1, x2)
    if x1 <= w1 and x2 >= w2:
        return (w1, w2)
    if x1 >= w1 and x1 <= w2:
        return (x1, w2)
    if x2 >= w1 and x2 <= w2:
        return (w1, x2)
    return None

def is3(c1, c2, on=False):
    xi = is1(c1.x1, c1.x2, c2.x1, c2.x2)
    if xi is None:
        return None
    yi = is1(c1.y1, c1.y2, c2.y1, c2.y2)
    if yi is None:
        return None
    zi = is1(c1.z1, c1.z2, c2.z1, c2.z2)
    if zi is None:
        return None
    return Cub(xi[0], xi[1], yi[0], yi[1], zi[0], zi[1], on)

# Algo:
#
# The idea is that if we go from the back, then all newly added cuboids
# only matter for the part that wasn't added yet - otherwise it gets
# overwritten.
#
# It doesnt matter what kind of cuboids were added before us - they "set"
# the state of the universe "after us" (remember, we're going from the back)
# so our parts that intersect with any of them don't matter.
#
# We iterate from the back, adding individual cuboids to the state.
# If it's a off cuboid, we just add it.
# If it's on:
# * walk over all pre-existing cuboids P
# * calculate intersection of C and P
# * this is the volume of C that doesnt matter: store it
# * for two pre-existing cuboids P1 and P2, there might be an overlap P1^P2
# * this overlap would count twice as extra volume, we need to store it
#
# example:
# - we're adding a "on" cuboid C
# - there are three preexisting cuboids P1, P2, P3
# * I1 = is(C, P1) (subtract)
# * I2 = is(C, P2) (subtract)
# * I1^I2 (add)
# * I3 = is(C, P3) (s)
# * I1^I3 (a)
# * I2^I3 (a)
# * I1^I3 ^ I2^I3 (s)


def getiss(c, cs):
    return list(filter(lambda x: x is not None, map(lambda a: is3(c, a), cs)))

def vol(c):
    return (c.x2-c.x1+1) * (c.y2-c.y1+1) * (c.z2-c.z1+1)

def getalliss(cs):
    all2 = itertools.combinations(cs, 2)
    return list(filter(lambda x: x is not None, map(lambda a: is3(a[0], a[1]), all2)))

def run():

    a = Cub(1, 10, 1, 10, 1, 10, None)
    b = Cub(0, 1, 0, 1, 0, 1, None)
    assert is3(a, b) == Cub(1,1,1,1,1,1,False)
    c = Cub(1, 10, 1, 10, 11, 12, None)
    assert is3(a, c) is None
    d = Cub(5, 7, 1, 10, 2, 13, None)
    assert is3(a, d, True) == Cub(5, 7, 1, 10, 2, 10, True)

    r = r'(on|off) x=([-0-9]+)..([-0-9]+),y=([-0-9]+)..([-0-9]+),z=([-0-9]+)..([-0-9]+)'
    cs = []
    for l in init():
        m = re.match(r, l).groups()
        cs.insert(0, Cub(int(m[1]), int(m[2]), int(m[3]), int(m[4]), int(m[5]), int(m[6]), m[0] == 'on'))

    p1 = True
    p1vol = Cub(-50, 50, -50, 50, -50, 50, False)

    ecs = []
    cnt = 0
    for c in cs:
        if p1:
            c = is3(c, p1vol, c.on)
        if not c:
            continue
        scs = getiss(c, ecs)
        ecs.insert(0, c)
        if not c.on:
            continue
        print('--- c=', c)
        print('--- scs=', scs)
        addsum = 0
        subsum = 0
        for ix, s in enumerate(scs):
            print('ins=', s)
            print(vol(s))
            subsum += vol(s)
            add = []
            subiss = getiss(s, scs[ix+1:])
            print('subiss=', subiss)
            for ss in subiss:
                # all that are not contained in |ss|, stay
                add = list(filter(lambda x: is3(x, ss) != x, add))
                # ss is added, if no other contains it
                cont = list(filter(lambda x: is3(x, ss) == ss, add))
                if len(cont) == 0:
                    add.append(ss)
                print('add=', add)
                ads = sum(map(vol, add))
                print('addsum=', ads)
            iss = getalliss(add)
            siss = getalliss(iss)
#            assert len(siss) == 0, (iss, siss)
            print('all iss=', iss)
            addsum += sum(map(vol, add))
            subsum += sum(map(vol, set(iss)))
        print('vol %d, adding %d, subbing %d' % (vol(c), addsum, subsum))
        cnt += vol(c) + addsum - subsum
        print('--- cnt=', cnt)

    print(len(ecs))
    print(cnt)

#    print(cs)
run()
