
use std::io::Read;


fn get_list() -> Vec<u32> {
    let mut buf = String::new();
    let mut stdin = std::io::stdin();
    stdin.read_to_string(&mut buf).expect("Failed to read data from stdin");
    let v = buf.lines().map(|x| x.parse().unwrap()).collect();
    v
}

fn main() {
    let l = get_list();

    let res1 = l.iter().zip(l.iter().skip(1)).filter(|(a,b)| b > a).count();
    let res2 = l.iter().zip(l.iter().skip(3)).filter(|(a,b)| b > a).count();

    println!("res1={}", res1);
    println!("res2={}", res2);
}
