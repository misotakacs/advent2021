
use std::io::Read;

#[derive(Debug)]
enum Move {
    Forward,
    Down,
    Up
}

fn get_list() -> Vec<(Move, u32)> {
    let mut buf = String::new();
    let mut stdin = std::io::stdin();
    stdin.read_to_string(&mut buf).expect("Failed to read data from stdin");
    let v = buf.lines()
               .map(|x| x.split(" ").collect())
               .map(|l: Vec<&str>| (match l[0] {
                   "forward" => Move::Forward,
                   "down" => Move::Down,
                   "up" => Move::Up,
                   _ => panic!()
               }, l[1].clone().parse().unwrap())).collect();
    v
}

fn main() {
    let l = get_list();

    let mut fc: u32 = 0;
    let mut dc: u32 = 0;
    let mut adc: u32 = 0;

    for (a, b) in l.iter() {
        match a {
            Move::Forward => { fc += b; adc += dc*b },
            Move::Up => dc -= b,
            Move::Down => dc += b
        }
    }

    println!("res1={:?}", fc*dc);
    println!("res2={:?}", fc*adc);
}
