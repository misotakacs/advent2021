import sys
from functools import reduce

def init():
    lines = []

    for line in sys.stdin:
        lines.append(line.strip())

    return lines

def run():
    ls = init()
    m = {}

    for l in ls[2:]:
        ps = l.split(' -> ')
        m[ps[0]] = ps[1]

    s = ls[0]

    extra = {}
    inc = {}
    for ix in range(len(s)-1):
        sub = s[ix:ix+2]
        inc[sub] = inc.get(sub, 0) + 1
        if ix > 0:
            extra[s[ix]] = 1

    print(s)
    print(inc)

    it = 40

    for _ in range(it):
        ninc = {}
        for k, v in inc.items():
            mk = m.get(k)
            if mk:
                ninc[k[0] + m[k]] = ninc.get(k[0] + m[k], 0) + v
                ninc[m[k] + k[1]] = ninc.get(m[k] + k[1], 0) + v
                extra[m[k]] = extra.get(m[k], 0) + v
        inc = ninc

    cs = {}
    for k, v in inc.items():
        cs[k[0]] = cs.get(k[0], 0) + v
        cs[k[1]] = cs.get(k[1], 0) + v

    for k, v in extra.items():
        cs[k] = cs[k] - v

    vs = list(cs.values())
    vs.sort()
    print(vs[-1] - vs[0])




run()

