import sys
import collections
import re

def getlines():
    ip = []
    for line in sys.stdin:
            ip.append(line.strip())

    return ip


def run():
    ls = getlines()

    ef = {}
    sf = {}
    es = {}    
    for y, l in enumerate(ls):
        for x, c in enumerate(l):
            if c == '>':
                ef[(x, y)] = True
            elif c == 'v':
                sf[(x, y)] = True
            elif c == '.':
                es[(x, y)] = True
                
    mx = len(ls[0])
    my = len(ls)

    def prn():
        for y in range(my):
            s = ''
            for x in range(mx):
                if (x, y) in es:
                    s += '.'
                elif (x, y) in sf:
                    s += 'v'
                elif (x, y) in ef:
                    s += '>'
                else:
                    assert False, '(%d, %d) has no data' % (x, y)
            print(s)
        print()

    move = True
    n = 0
    while move:
        #prn()
        n += 1
        nef = {}
        nsf = {}
        nes = {}
        move = False
        for (x, y) in es.keys():
            efp = ((x - 1) % mx, y)
            if efp in ef:
                move = True
                nef[(x, y)] = True                
                del ef[efp]
                
                sfp = (efp[0], (efp[1] - 1) % my)
                if sfp in sf:                
                    nsf[efp] = True
                    nes[sfp] = True
                    del sf[sfp]
                else:
                    nes[efp] = True    

                continue

            sfp = (x, (y - 1) % my)
            if sfp in sf:
                move = True
                nsf[(x, y)] = True
                nes[sfp] = True
                del sf[sfp]
                continue

            nes[(x, y)] = True

        es = nes
        nsf.update(sf)
        sf = nsf
        nef.update(ef)
        ef = nef

    print(n)
            
            


run()
