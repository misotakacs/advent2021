import sys

def getinput():
	lines = []
	for line in sys.stdin:
		lines.append(line.strip())
	return lines

def run():
	iss = getinput()
	ls = []
	for l in iss:
		ps = l.split(' -> ')
		ls.append(
			(
				(int(ps[0].split(',')[0]), (int(ps[0].split(',')[1]))), 
			    (int(ps[1].split(',')[0]), (int(ps[1].split(',')[1])))
			)
		)
	
	h = filter(lambda l: l[0][1] == l[1][1], ls)
	v = filter(lambda l: l[0][0] == l[1][0], ls)

	inc = {}
	for hl in h:
		y = hl[0][1]
		if hl[0][0] > hl[1][0]:
			hl = ((hl[1][0], y), (hl[0][0], y))
		for x in range(hl[0][0], hl[1][0]+1):
			inc[(x, y)] = inc.get((x, y), 0) + 1

	for vl in v:
		x = vl[0][0]
		if vl[0][1] > vl[1][1]:
			vl = ((x, vl[1][1]), (x, vl[0][1]))
		for y in range(vl[0][1], vl[1][1]+1):
			inc[(x, y)] = inc.get((x, y), 0) + 1

	cnt = 0
	for v in inc.values():
		if v > 1:
			cnt += 1

	print cnt

run()
