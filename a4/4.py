import sys
import re

def init():
    lines = []

    for line in sys.stdin:
        lines.append(line.strip())

    nums = map(lambda x: int(x), lines[0].split(','))

    ix = 2
    done = False

    bs = []

    while ix < len(lines):
        iix = 0
        b = []
        while iix < 5:    
            b.append(map(lambda x: int(x), re.split('\s+', lines[ix])))
            iix += 1
            ix += 1

        bs.append(b)

        ix += 1

    hs = [
        [[0 for _ in range(5)] for _ in range(5)] for _ in range(len(bs))
    ]

    return nums, bs, hs

def mark(num, b, h):
    for rx, r in enumerate(b):
        for cx, c in enumerate(r):
            if c == num:
                h[rx][cx] = 1

def check(h):
    cvs = [0] * 5
    for rx, r in enumerate(h):
        rv = 0
        for cx, c in enumerate(r):
            rv += c
            cvs[cx] += c
            if cvs[cx] == 5:
                return True

        if rv == 5:
            return True

    return False
    

def unmarkedsum(b, h):
    s = 0
    for rx, r in enumerate(b):
        for cx, c in enumerate(r):
            if h[rx][cx] != 1:
                s += c
    return s

(nums, bs, hs) = init()

won = {}
first = True

for num in nums:
    for ix, b in enumerate(bs):
        mark(num, b, hs[ix])
        if not ix in won and check(hs[ix]):
            if first:
                print("First: %d with number %d" % (ix, num))
                s = unmarkedsum(b, hs[ix])
                print("Sum = %d, %d * %d = %d" % (s, s, num, s*num))
                first = False
            won[ix] = True
            if len(won) == len(bs):
                print("Last: %d with number %d" % (ix, num))
                s = unmarkedsum(b, hs[ix])
                print("Sum = %d, %d * %d = %d" % (s, s, num, s*num))
                first = False
                exit(1)

    
