import sys

def init():
    lines = []

    for line in sys.stdin:
        lines.append(line.strip())

    return lines

def run():

    ls = init()
    sp = [int(s.split(': ')[1]) - 1 for s in ls[0:2]]

    p = sp.copy()

    print(p)

    pts = [0] * 2

    cnt = 1    
    while max(pts) < 1000:
        for ix in range(2):
            s = 0
            for _ in range(3):
                s += cnt
                cnt += 1
            
            p[ix] = (p[ix] + s) % 10            
            pts[ix] += (p[ix] + 1)

            if pts[ix] >= 1000:
                loser = (ix+1)%2
                print('Die was rolled %d times, loser has %d points' % (cnt-1, pts[loser]))
                print('Part1: %d' % ((cnt -1) * pts[loser]))
                break

    # number of universes generates when total points are rolled    
    mp = {
        3: 1,
        4: 3,        
        5: 6,
        6: 7,
        7: 6,
        8: 3,
        9: 1
    }

    assert sum(mp.values()) == 27

    def mku(p, pt, u, ix):        
        w = [0, 0]
        for n, v in mp.items():
            np = (p[ix] + n) % 10
            npt = pt[ix] + np + 1
            if npt >= 21:                
                w[ix] += u * v
                continue                
            if ix == 0:                
                nw = mku((np, p[1]), (npt, pt[1]), u * v, 1)   
            else:
                nw = mku((p[0], np), (pt[0], npt), u * v, 0)                
            
            w[0] += nw[0]
            w[1] += nw[1]
        return w

    us = mku((sp[0], sp[1]), (0, 0), 1, 0)
    print('Part 2: ', us)

run()