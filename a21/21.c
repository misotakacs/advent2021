#include <stdio.h>
#include <stdint.h>

char nums[] = {3, 4, 5, 6, 7, 8, 9};
char uss[] = {1, 3, 6, 7, 6, 3, 1};

#define NUMS 7

typedef struct ws {
    uint64_t p1;
    uint64_t p2;
} wins;

wins mku(char p1, char p2, char p1pt, char p2pt, uint64_t u, char ix) {
    char pos = p1;
    char pts = p1pt;
    wins ws;
    ws.p1 = 0;
    ws.p2 = 0;
    if (ix == 1) {
        pos = p2;
        pts = p2pt;
    }
    char i;
    for (i = 0; i < NUMS; i++) {
        char npos = (pos + nums[i]) % 10;
        char npts = pts + npos + 1;
        if (npts >= 21) {
            if (ix == 0) {
                ws.p1 += u*uss[i];
            } else {
                ws.p2 += u*uss[i];
            }
            continue;
        }
        wins r;
        if (ix == 0) {
            r = mku(npos, p2, npts, p2pt, u * uss[i], 1);
        } else {
            r = mku(p1, npos, p1pt, npts, u * uss[i], 0);
        }
        ws.p1 += r.p1;
        ws.p2 += r.p2;
    }

    return ws;
}

int main(int argc, char **argv) {
    wins r = mku(5, 7, 0, 0, 1, 0);
    printf("%ld, %ld\n", r.p1, r.p2);
}