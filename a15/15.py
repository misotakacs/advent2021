import sys

def init():
    lines = []

    for line in sys.stdin:
        lines.append(line.strip())

    return lines

def run():
    ls = init()

    m = []
    for l in ls:
    	m.append([int(c) for c in l])

    mul = 5

    mm = m[-1][-1] + (mul * 2) - 2
    if mm > 9:
    	mm -= 9
    a = (mul*len(m[0]) - 1, mul*len(m) - 1, mm)
    vis = {}
    q = [a]
    d = [(-1, 0), (1, 0), (0, -1), (0, 1)]
    rm = [[1e10] * len(m[0] * mul) for _ in range(mul * len(m))]

    ix = 0

    while len(q) > 0:
    	(x, y, l) = q.pop(0)
	vis[(x, y)] = True
	if rm[y][x] < l:
		continue
	rm[y][x] = l
	ix += 1
	for (dx, dy) in d:
		nx = x + dx
		ny = y + dy
		px = nx / len(m[0])
		py = ny / len(m)
		if nx < 0 or ny < 0 or nx >= mul*len(m[0]) or ny >= mul*len(m):
			continue
		nm = m[ny % len(m)][nx % len(m[0])] + px + py
		if nm > 9:
			nm -= 9
		if rm[ny][nx] <= l+nm:
			continue
		rm[ny][nx] = l+nm
		q.append((nx, ny, l+nm))

    print rm[0][0] - m[0][0]

run()
