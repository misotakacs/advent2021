import sys

def getinput():
    lines = []
    for line in sys.stdin:
        lines.append(line.strip())
    return lines

def run():

    ls = getinput()
    nums = map(lambda x: int(x), ls[0].split(','))

    avgpos = int(sum(nums) / len(nums))

    delta = 0
    mind = 1e8
    minpos = 0

    ns = lambda n: (n*(n+1)) / 2

    while delta < 50:
        td = sum(map(lambda x: ns(abs(avgpos + delta - x)), nums))        
        if td < mind:
            mind = td
            minpos = avgpos + delta

        if delta > 0:
            td = sum(map(lambda x: ns(abs(avgpos - delta - x)), nums))            
            if td < mind:
                mind = td
                minpos = avgpos - delta

        delta += 1

    print(minpos, mind)

run()