import sys
import collections

def getlines():
    ip = []
    for line in sys.stdin:
            l = line.strip()
            ip.append([int(e) for e in l])

    return ip

def bsize(ip, x, y):
    q = [(x, y)]
    visited = {}

    d = [(-1, 0), (1, 0), (0, -1), (0, 1)]

    while len(q) > 0:
        (x, y) = q.pop()
        visited[(x, y)] = True

        for dt in d:
            nx = x + dt[0]
            ny = y + dt[1]

            if ny < 0 or ny >= len(ip) or nx < 0 or nx >= len(ip[0]):
                continue
            if ip[ny][nx] == 9:
                continue
            if visited.get((nx, ny), False):
                continue
            q.append((nx, ny))

    return len(visited)

def run():
    ip = getlines()

    s = 0

    basins = []

    for y, r in enumerate(ip):
        for x, v in enumerate(r):
            ismin = True
            if y > 0:
                ismin = ismin and v < ip[y-1][x]
            if x > 0:
                ismin = ismin and v < ip[y][x-1]
            if y < len(ip) - 1:
                ismin = ismin and v < ip[y+1][x]
            if x < len(r) - 1:
                ismin = ismin and v < ip[y][x+1]
        
            if ismin:
                basins.append(bsize(ip, x, y))
                s += v+1

    print(s)
    basins.sort(reverse=True)
    print(reduce(lambda a, b: a* b, basins[:3], 1))

run()

