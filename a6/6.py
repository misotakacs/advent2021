import sys

def getinput():
    lines = []
    for line in sys.stdin:
        lines.append(line.strip())
    return lines

def run():

    ls = getinput()
    nums = map(lambda x: int(x), ls[0].split(','))

    print(nums)

    ds = {}

    for n in nums:
        ds[n] = ds.get(n, 0) + 1 

    days = 256

    while days > 0:
        nds = {}
        for (v, c) in ds.iteritems():
            if v == 0:
                nds[8] = nds.get(8, 0) + c
                nds[6] = nds.get(6, 0) + c
            else:
                nds[v-1] = nds.get(v-1, 0) + c
        ds = nds
        days -= 1

    print(reduce(lambda a, b: a+b, ds.values(), 0))


run()