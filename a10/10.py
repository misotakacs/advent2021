import sys
import collections

def getlines():
    ip = []
    for line in sys.stdin:
            ip.append(line.strip())

    return ip

def run():
  ip = getlines()

  m = {
    ']': '[',
    '}': '{',
    ')': '(',
    '>': '<',
  }

  # reverse mapping
  m2 = {b:a for (a,b) in m.iteritems()}

  # p1 scores
  v = {
    ']': 57,
    ')': 3,
    '}': 1197,
    '>': 25137,
  }

  # p2 scores
  v2 = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4,
  }

  s = 0
  s2 = []
  for l in ip:
    # stack
    st = []
    for e in l:
      skip = False
      if e in ['[', '{', '<', '(']:
        st.append(e)
      else:
        t = st.pop()
        # is it the correct closing one?
        if t != m[e]:
          s += v[e]
          skip = True
          break
    if not skip:
      # the rest of the stack has the correct replacements
      st.reverse()
      s2.append(reduce(lambda a,b: a*5 + b, map(lambda x: v2[m2[x]], st), 0)) 
  
  print(s)

  s2.sort()
  print(s2[len(s2)//2])
              

run()
