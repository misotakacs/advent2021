import sys
import re
import math
import itertools
from functools import reduce

def init():
    lines = []

    for line in sys.stdin:
        lines.append(line.strip())

    return lines


class elm:

    def __init__(self, v=None):
        self.v = v
        self.l = None
        self.r = None

    def __str__(self):
        if self.v is not None:
            return str(self.v)
        return '[' + str(self.l) + ',' + str(self.r) + ']'

    def mag(self):
        if self.v is not None:
            return self.v
        return 3*self.l.mag() + 2*self.r.mag()

    def copy(self):
        e = elm(self.v)
        if self.v is None:
            e.l = self.l.copy()
            e.r = self.r.copy()
        return e

def parse(s):
    re = elm()
    first = True
    num = None
    ix = 0
    while True:
        c = s[ix]   
        if c == ']':
            if num is not None:
                re.r = elm(num)                
            return re, ix
        elif c == '[':
            e, nix = parse(s[ix+1:])
            ix = ix+nix+1
            if first:
                re.l = e                
            else:
                re.r = e                
        elif c == ',':
            first = False
            if num is not None:                
                re.l = elm(num)
            num = None
        else:
            # numbers
            if num is None:
                num = 0
            num = num*10 + int(c)           

        ix += 1

    assert False, 'This should not be reached!'


def explode(start):
    l = 0
    f = None
    st = [(start, 0)]
    lastnum = None
    nextnum = None
    while st:
        e, l = st.pop()
        if e.v is None:
            if f is None and l == 4 and e.l.v is not None and e.r.v is not None:
                f = e
            else:
                st.append((e.r, l+1))
                st.append((e.l, l+1))
        else:
            if f is None:
                lastnum = e
            else:
                nextnum = e
                break
        
    if f is None:
        return False

    if lastnum:
        lastnum.v += f.l.v
    if nextnum:
        nextnum.v += f.r.v

    f.v = 0
    f.l = None
    f.r = None

    return True

def split(start):
    l = 0
    f = None
    st = [start]
    while st:
        e = st.pop()        
        if e.v is not None:
            if e.v >= 10:
                f = e
                break
        else:            
            st.append(e.r)
            st.append(e.l)
        
    if f is None:
        return False

    f.l = elm(int(f.v/2.0))
    f.r = elm(int(math.ceil(f.v/2.0)))
    f.v = None

    return True
            

def add(a, b):
    n = elm()
    n.l = a
    n.r = b

    while explode(n) or split(n):
        pass

    return n


def run():
    sns = [parse(s[1:])[0] for s in init()]

    a = reduce(lambda a, b: add(a.copy(), b.copy()), sns)

    print('magnitude of total sum: %d' % a.mag())

    print('max magnitude: %d' % max([add(s[0].copy(), s[1].copy()).mag() for s in itertools.permutations(sns, 2)]))

run()
