import sys
import itertools

def test(ass, ws, nums, lens):
	# done
	if len(ws) == 0:
		return ass
	w = ws[0]
	# subset of "found" positions that exist in the word
	sub = filter(lambda x: x in w, ass.keys())	
	# iterate through all of the 
	for n in lens[len(w)]:
		# do they match any n-lettered word?
		ex = filter(lambda x: x not in nums[n], map(lambda x: ass[x], sub))
		# full match
		if len(ex) == 0:
			# missing letters
			ml = set([c for c in w]) - set(sub)
			# missing positions
			mp = set(nums[n]) - set(ass.values())
			if len(mp) >= len(ml):
				for ix, c in enumerate(itertools.permutations(mp, len(ml))):
					nass = dict(zip(ml, c))
					as2 = ass.copy()
					as2.update(nass)
					a = test(as2, ws[1:], nums,lens)
					if a is not None:
						return a
				
	return None



def run():
	ip = []

	for line in sys.stdin:
		l = line.strip()
		ip.append(map(lambda e: e.split(" "), l.split(" | ")))

	cnt = 0
	for i in ip:
		cnt += sum([1 if len(e) in [2,3,4,7] else 0 for e in i[1]])

	print "#1: ", cnt

	nums = {
		0: [0,1,2,4,5,6],
		1: [2,5],
		2: [0,2,3,4,6],
		3: [0,2,3,5,6],
		4: [1,2,3,5],
		5: [0,1,3,5,6],
		6: [0,1,3,4,5,6],
		7: [0,2,5],
		8: [0,1,2,3,4,5,6],
		9: [0,1,2,3,5,6],
		}
	lens = {}
	for n, ss in nums.items():
		ls = lens.get(len(ss), [])
		ls.append(n)
		lens[len(ss)] = ls

	sm = 0		
	for ix, i in enumerate(ip):
		i[0].sort(key=lambda x: len(x))
		code = test({}, i[0], nums,lens)
		subsum = 0	
		for w in i[1]:
			ns = set([code[c] for c in w])
			fs =  [n for n in lens[len(w)] if set(nums[n]) == ns]
			assert len(fs) == 1
			subsum = subsum*10 + fs[0]

		sm += subsum

	print "#2: ", sm



run()
