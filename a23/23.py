import sys
import re
import itertools
import collections

def init():
    lines = []

    for line in sys.stdin:
        lines.append(line.strip())

    return lines

class Amp:
    def __init__(self, name, x, y, cost):
        self.name = name
        self.x = x
        self.y = y
        self.cost = cost

    def __repr__(self):
        return self.name

Pos = collections.namedtuple('Pos', ('x', 'y'))
Move = collections.namedtuple('Move', ('amph', 'start', 'dest', 'cost'))

EMPTY = '.'

# y coordinates of levels
HALL = 1
ROOM1 = 2
ROOM2 = 3
ROOM4 = 5

# allowed x coordinates in hall
HALL_ALLOWED = [1,2,4,6,8,10,11]

class State:    
    roomx = {
        'A': 3,
        'B': 5,
        'C': 7,
        'D': 9,
    }

    def __init__(self, p1):
        self.map = {}
        self.amph = []
        self.visited = {}
        self.mincost = 1e5
        self.it = 0
        self.p1 = p1

    # Generate all possible moves.
    def moves(self):
        # a move is (amph, pos1, pos2)

        if self.p1:
            maxroom = ROOM2
        else:
            maxroom = ROOM4

        moves = [] 
        for a in self.amph:
            roomx = self.roomx[a.name]
            # in a room            
            if a.y > HALL:
                movable = True            
                for y in range(a.y-1, ROOM1-1, -1):
                    if self.map[(a.x, y)] != EMPTY:
                        movable = False
                        break
                stable = True
                for y in range(a.y + 1, maxroom+1):
                    # only buddies below me
                    if self.map[(a.x, y)] != a.name:
                        stable = False                
                if a.x == roomx and a.y == maxroom:
                    # already home!
                    continue
                if a.x == roomx and stable:
                    # home and my buddy is also home! not going anywhere
                    continue
                # decide if it's possible to go into destination room
                # for that, it must be empty, or filled solely with correct amphs                
                lastempty = -1
                room_feasible = True
                for y in range(ROOM1, maxroom+1):
                    if self.map[(roomx, y)] != EMPTY and self.map[(roomx, y)] != a.name:
                        room_feasible = False
                        break
                    if self.map[(roomx, y)] == EMPTY:
                        lastempty = y

                # we know the |movable| state and |room_feasible| state
                if movable and room_feasible:
                    step = 1 if roomx > a.x else -1
                    reachable = True
                    for x in range(a.x+step, roomx+step, step):
                        if self.map[(x, HALL)] in 'ABCD':
                            reachable = False
                            break
                    if reachable:
                        moves.append(Move(a, Pos(a.x, a.y), Pos(roomx, lastempty),
                                          a.cost * (a.y-1+abs(a.x-roomx)+lastempty-1)))
                    
                # if an amph is in the room, it can move into any accessible hallway position
                if movable:
                    # moving left
                    for x in range(a.x-1, min(HALL_ALLOWED)-1, -1):
                        if x in HALL_ALLOWED and self.map[x, HALL] == EMPTY:
                            moves.append(Move(a, Pos(a.x, a.y), Pos(x, HALL), a.cost * (a.x-x + (a.y-1))))
                        elif self.map[(x, HALL)] in 'ABCD':
                            # oops someone there
                            break
                    for x in range(a.x+1, max(HALL_ALLOWED)+1):
                        if x in HALL_ALLOWED and self.map[x, HALL] == EMPTY:
                            moves.append(Move(a, Pos(a.x, a.y), Pos(x, HALL), a.cost * (x-a.x + (a.y-1))))
                        elif self.map[(x, HALL)] in 'ABCD':
                            break
            else: # in the hall            
                step = 1 if roomx > a.x else -1
                reachable = True
                for x in range(a.x+step, roomx+step, step):
                    if self.map[(x, HALL)] in 'ABCD':
                        reachable = False
                        break
                if not reachable:
                    continue
                # decide if it's possible to go into destination room
                # for that, it must be empty, or filled solely with correct amphs                
                lastempty = -1
                room_feasible = True
                for y in range(ROOM1, maxroom+1):
                    if self.map[(roomx, y)] != EMPTY and self.map[(roomx, y)] != a.name:
                        room_feasible = False
                        break
                    if self.map[(roomx, y)] == EMPTY:
                        lastempty = y
                if room_feasible:
                    moves.append(Move(a, Pos(a.x, a.y), Pos(roomx, lastempty), a.cost*(lastempty-1+abs(a.x-roomx))))
                

        moves.sort(key=lambda x:x.cost)
        return moves


    def final(self):
        for a in self.amph:
            if a.x != State.roomx[a.name]:
                return False

        return True

    def optimal(self, cost):
        hash = ''
        for ix, a in enumerate(self.amph):
            hash += '%s-%d(%d,%d)' % (a.name, ix, a.x, a.y)        
        if hash not in self.visited or cost < self.visited[hash]:
            self.visited[hash] = cost
            return True
        return False
        
    def print(self):
        pass

    def solve(self, cost, moves, depth=0):        
        for ix, m in enumerate(moves):
            if cost+m.cost > self.mincost:
                continue
            m.amph.x = m.dest.x
            m.amph.y = m.dest.y
            # verify solution
            isFinal = self.final()
            if not isFinal and not self.optimal(cost+m.cost):
                m.amph.x = m.start.x
                m.amph.y = m.start.y
                continue

            self.map[(m.start.x, m.start.y)] = EMPTY            
            self.map[(m.dest.x, m.dest.y)] = m.amph.name
                        
            if not isFinal:                
                self.solve(cost + m.cost, self.moves(), depth+1)
            # reverse move
            self.map[(m.start.x, m.start.y)] = m.amph.name
            m.amph.x = m.start.x
            m.amph.y = m.start.y
            self.map[(m.dest.x, m.dest.y)] = EMPTY

            if isFinal and self.mincost > cost+m.cost:
                self.mincost = cost+m.cost
                print('[%d] Solved in %d' % (depth, self.mincost))
                break

        return -1


def run():
    m = init()

    s = State(len(m) == 5)
    for y, r in enumerate(m):        
        for x, c in enumerate(r):                        
            s.map[(x, y)] = c
            if c in 'ABCD':                              
                s.amph.append(Amp(c, x, y, 10**(ord(c) - ord('A'))))
    
    s.solve(0, s.moves())
    print(s.mincost)

run()