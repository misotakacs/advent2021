import sys

def init():
    lines = []

    for line in sys.stdin:
        lines.append(line.strip())

    return lines

class stream:
    def __init__(self, data):
        self.data = data
        self.pos = 0

    def getint(self, bits):

        ret = 0

        assert(self.pos + bits <= len(self.data) * 4)

        while bits > 0:
            curr = int(self.data[self.pos/4], 16)     # the single byte we're operating in
            start = self.pos % 4    # the start bit within the byte
            if bits > 4 - start:    # if we want more bits than are left in current byte
                nb = 4 - start
            else:
                nb = bits

            # need to shift the bits to the right
            # and clear unneeded top bits
            ret = (ret << nb) + ((curr >> (4 - start - nb)) & (pow(2, nb)-1))

            self.pos += nb
            bits -= nb

        #print('ret=%d' % ret)
        return ret

class packet:

    LIT = 4
    SUM = 0
    PRODUCT = 1
    MIN = 2
    MAX = 3
    GT = 5
    LT = 6
    EQ = 7

    def __init__(self, ver, tp):
        self.ver = ver
        self.tp = tp
        self.val = 0
        self.vallen = 0
        self.i = 0
        self.sub = []

    def len(self):
        l = 6
        if self.tp == packet.LIT:
            l += self.vallen
        else:
            l += 1
            if self.i == 0:
                l += 15
            else:
                l += 11
            l += self.sublen()
        return l

    def sublen(self):
        return sum([p.len() for p in self.sub])

    def versum(self):
        return self.ver + sum([p.versum() for p in self.sub])

    def eval(self):
        if self.tp == packet.SUM:
            return sum([p.eval() for p in self.sub])
        if self.tp == packet.PRODUCT:
            return reduce(lambda a, b: a*b, [p.eval() for p in self.sub], 1)
        if self.tp == packet.MIN:
            return min([p.eval() for p in self.sub])
        if self.tp == packet.MAX:
            return max([p.eval() for p in self.sub])
        if self.tp == packet.GT:
            return 1 if self.sub[0].eval() > self.sub[1].eval() else 0
        if self.tp == packet.LT:
            return 1 if self.sub[0].eval() < self.sub[1].eval() else 0
        if self.tp == packet.EQ:
            return 1 if self.sub[0].eval() == self.sub[1].eval() else 0
        if self.tp == packet.LIT:
            return self.val

    def __str__(self):
        s = 'Ver: %d, type: %d\n' % (self.ver, self.tp)
        if self.tp == packet.LIT:
            s += 'val: %d\n' % self.val
        else:
            s += 'i=%d\n' % self.i
            s += '---- subs ----\n'
            for p in self.sub:
                s += str(p)

        return s

def parse(s, level=0):
    print('--- level: %d ---' % level) 
    p = packet(s.getint(3), s.getint(3))
    if p.tp == packet.LIT:
        p.val = 0
        p.vallen = 0
        print('-- lit packet start --')
        while True:
            num = s.getint(5)
            p.vallen += 5
            p.val = (p.val << 4) + (num & 0b01111)
            if num & 0b10000 == 0:
                print('-- lit packet finish: %d --' % p.val)
                return p
    else:
        lt = s.getint(1)
        p.i = lt
        if lt == 0:
            l = s.getint(15)
            print('-- L-type op: %d bits ---' % l)
            while p.sublen() < l:
                print('p.len=%d, l=%d' % (p.len(), l))
                p.sub.append(parse(s, level+1))
            print('--- [%d] done L-type op packet; len=%d  ---' % (level, p.len()))
        else:
            np = s.getint(11)
            print('-- CNT-type op: %d packets ---' % np)
            for ix in range(np):
                print('-- [%d] before %d. packet parse --' % (level, ix))
                p.sub.append(parse(s, level + 1))
            print('--- [%d] done CNT-type op packet ---' % level)

        return p

def run():
    ls = init()
    line = ls[0]

    print(line)
    s = stream(line)
    p = parse(s)

    print(p)
    print(p.versum())

    print(p.eval())

run()
