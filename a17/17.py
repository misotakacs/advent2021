import sys
import re
import math

def init():
    lines = []

    for line in sys.stdin:
        lines.append(line.strip())

    return lines

def trange(vy, y1, y2):
    # Calculate the time points at which the function crosses
    # the (y1, y2) range.
    # vy(0) = vy
    # vy(1) = vy - 1
    # vy(2) = vy - 2
    # vy(3) = vy - 3
    #
    # y(0) = vy(0)
    # y(1) = vy(0) + vy(1) = vy + vy - 1
    # y(2) = vy(0) + vy(1) + vy(2) = vy + vy - 1 + vy - 2
    # y(t) = (t+1) * vy - (t*(t+1)/2)

    # Y = (t+1)*vy0 - t^2/2 - t/2
    # 0 = t^2/2 + t(vy0 - 1/2) + vy0 - Y
    #
    # ax^2 + bx + c = 0
    # D = b^2 - 4*a*c
    # x1 = (-b - sqrt(D)) / 2*a
    # x2 = (-b + sqrt(D)) / 2*a

    def solve(vy, Y):
        a = -0.5
        b = vy - 0.5
        c = vy-Y
        D = b*b - 4*a*c
        if D < 0:
            return None
        # the larger root
        return (-b - math.sqrt(D)) / (2*a)

    t1 = solve(vy, y1)
    t2 = solve(vy, y2)
    
    if t1 <= 0:
        t1 = 0
    if t2 <= 0:
        t2 = 0

    if t1 > t2:
        t1, t2 = t2, t1

    if int(t1) != 0:
        t1d = t1 % int(t1)
        t1i = int(t1) if t1d == 0 else int(t1)+1
    else:
        t1i = 0

    if int(t2) != 0:
        t2d = t2 % int(t2)
        t2i = int(t2) if t2d == 0 else int(t2)-1
    else:
        t2i = 0

    return (t1i, t2i)

def fy(vy, t):
    return (t+1)*vy - (t*(t+1)/2)

def fx(vx, t):
    if t <= vx:
        return (t+1)*vx - (t*(t+1)/2)
    else:
        return (vx+1)*vx - (vx*(vx+1)/2)

def run():
    l = init()[0]
    p = r'.*x=(-?[0-9]+)..(-?[0-9]+), y=(-?[0-9]+)..(-?[0-9]+)'
    m = re.match(p, l)
    (x1, x2, y1, y2) = [int(x) for x in m.groups()]

    if x1>x2:
        x1, x2 = x2, x1
    if y1>y2:
        y1, y2 = y2, y1

    maxy = 0
    cnt = 0

    for vx in range(1, 200):
        for vy in range(-200, 200):
            t1, t2 = trange(vy, y1, y2)
            for t in range(t1, t2+2):
                x = fx(vx, t)
                y = fy(vy, t)
                if x >= x1 and x <= x2 and y >= y1 and y <= y2:
                    mt = vy - t
                    # max y can be calculates from the 1st derivative.
                    my = mt*(mt+1) - (mt*(mt+1)/2)
                    if my > maxy:
                        maxy = my
                    cnt += 1
                    break

            continue

    print(maxy)
    print(cnt)



run()
