import sys
import collections
from PIL import Image, ImageDraw

def getlines():
    ip = []
    for line in sys.stdin:
            l = line.strip()
            ip.append(l)

    return ip

class pt:
    def __init__(self, x, y, r=1):
        self.x = x
        self.y = y
        self.r = r
    
    def __str__(self):
        return '(%d, %d)' % (self.x, self.y)


def frame(pts):
    minx = 1e5
    miny = 1e5
    maxx = -1e5
    maxy = -1e5

    for p in pts:
        if p.x < minx:
            minx = p.x
        if p.y < miny:
            miny = p.y
        if p.x > maxx:
            maxx = p.x
        if p.y > maxy:
            maxy = p.y

    return (minx, miny, maxx, maxy)

def mappts(pts, w, h):
    npts = set()
    f = frame(pts)
    fw = f[2] - f[0]
    fh = f[3] - f[1]

    if float(w)/h > float(fw)/fh:
        k = h / float(fh)
    else:
        k = w / float(fw)

    for p in pts:
        npts.add(pt(
            int((p.x - f[0]) * k),
            int((p.y - f[1]) * k),
            round(k/2)))

    return npts


def foldup(pts, y, steps=10):
    aff = set(filter(lambda p: p.y > y, pts))
    naff = pts.difference(aff)    
    fs = []
    for i in range(1, steps+1):
        new = naff.copy()
        frac = i / float(steps)
        for p in aff:
            d = int(frac * ((p.y - y) * 2))
            new.add(pt(p.x, p.y - d))
        fs.append(new)

    return fs

def foldleft(pts, x, steps=10):
    aff = set(filter(lambda p: p.x > x, pts))
    naff = pts.difference(aff)    
    fs = []
    for i in range(1, steps+1):
        new = naff.copy()
        frac = i / float(steps)
        for p in aff:
            d = int(frac * ((p.x - x) * 2))
            new.add(pt(p.x - d, p.y))
        fs.append(new)

    return fs

def run():
    ip = getlines()

    pts = set()
    folds = None
    for l in ip:
        if l == "":
            folds = []
            continue
        
        if folds is None:
            ps = l.split(',')
            pts.add(pt(int(ps[0]), int(ps[1])))
        else:
            fs = l.split('=')
            folds.append((fs[0][-1:], int(fs[1])))

    fs = []
    for f in folds:
        if f[0] == 'y':
            fs.extend(foldup(pts, f[1]))
        else:
            fs.extend(foldleft(pts, f[1]))

        pts = fs[-1]        

    w = 1500
    h = 1000
    bgc = (0, 0, 0)
    c = (100, 50, 50)

    ims = []
    for f in fs:
        im = Image.new('RGB', (w, h), bgc)
        draw = ImageDraw.Draw(im)
        for p in mappts(f, w-40, h-40):
            draw.ellipse((20+ p.x - p.r, 20+ p.y - p.r, 20+ p.x + p.r, 20 + p.y + p.r), fill=c)
        ims.append(im)

    for _ in range(200):
        ims.append(ims[-1])

    print('saving....')
    ims[0].save('fold.gif',
              save_all = True, append_images = ims[1:],
              optimize = False, duration=80)


run()

