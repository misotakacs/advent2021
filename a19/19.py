import sys
import re
import itertools
import collections

def init():
    lines = []

    for line in sys.stdin:
        lines.append(line.strip())

    return lines

Pos = collections.namedtuple('Pos', ('x', 'y', 'z'))
Ds = collections.namedtuple('Ds', ('x', 'y', 'z'))

class Scanner:
    def __init__(self, number, beacons):
        self.number = number
        self.beacons = beacons
        #self.diffs = self.makediffs(beacons)
        self.deltas = [0, 0, 0]


    def dimdiffs(beacons, dim, mul=1):        
        dimvals = [(b[dim]*mul, b, mul) for b in beacons]        
        assert(len(dimvals) == len(set(dimvals)))
        dimvals.sort(key=lambda x: x[0])
        diffs = []
        for (d, _, _) in dimvals:
            ds = [(d - val, b, mul) for (val, b, mul) in dimvals]
            ds.sort(key=lambda x: x[0])
            diffs.append(ds)

        return diffs


    def makediffs(self, beacons):
        ds = [(Scanner.dimdiffs(beacons, dim), Scanner.dimdiffs(beacons, dim, -1)) for dim in range(3)]
        return ds

def overlap(a1, a2):
    ol = []
    ix1 = 0
    ix2 = 0
    while ix1 < len(a1) and ix2 < len(a2):        
        if a1[ix1][0] > a2[ix2][0]:
            ix2+=1
        elif a1[ix1][0] < a2[ix2][0]:
            ix1+=1
        else:
            ol.append((a1[ix1][0], a1[ix1][1], a2[ix2][1],
                      (a1[ix1][1][0]-a2[ix2][1][0], a1[ix1][1][1]-a2[ix2][1][1], a1[ix1][1][2]-a2[ix2][1][2])))
            # assumption: no identical coordinates (we assert this) in dimdiffs
            ix1+=1
            ix2+=1    
    return ol

def fix(s1, s2, d1, d2, m1, m2, ol):
    #print('fixing: %s' % ol)
    # need to rotate s2 to achieve full match
    # we know that d2 is really d1
    dims = {
        d1: d2,        
    }

    inv = {
        d1: m2,
    }

    deltas = {
        d1: ol[0][1][d1] - ol[0][2][d2]*inv[d1],
    }

    # sooo ugly!
    for dim1 in range(3):        
        if dim1 in dims:
            continue
        for dim2 in range(3):
            if dim2 in dims.values():
                continue
            # test the dim1->dim2 mapping
            # in both directions
            match = None
            matchInv = None
            delta = 0
            deltaInv = 0
            for o in ol[:4]:
                d = o[1][dim1] - o[2][dim2]
                di = o[1][dim1] - o[2][dim2]*-1
                if match is None:
                    delta = d
                    deltaInv = di
                    match = True
                    matchInv = True
                else:
                    if d != delta:
                        match = False
                    if di != deltaInv:
                        matchInv = False
            assert not match or not matchInv
            if match or matchInv:
                dims[dim1] = dim2
                inv[dim1] = -1 if matchInv else 1
                deltas[dim1] = delta if match else deltaInv

    if len(dims) < 3:
        return None
    #print(deltas)
    bs = []
    for b in s2.beacons:
        nb = Pos((b[dims[0]]*inv[0]) + deltas[0],
                   (b[dims[1]]*inv[1]) + deltas[1],
                   (b[dims[2]]*inv[2]) + deltas[2])
        bs.append(nb)
    s = Scanner(s2.number, bs)
    s.deltas = deltas
    return s

# Newer, faster way.
def altfind(s1, s2):
    b1m = {b[0]: b for b in s1.beacons}
    res = []
    for b1 in s1.beacons:
        for b2 in s2.beacons:
            for d2 in range(3):
                for m in [1, -1]:
                    res = []
                    delta = b2[d2]*m - b1[0]
                    for ix, b2 in enumerate(s2.beacons):
                        newv = b2[d2]*m - delta
                        if newv in b1m:
                            res.append((0, b1m[newv], b2))
                            if len(s2.beacons) - ix - 1 < 12 - len(res):
                                # no need to continue: can't reach 12 any more
                                break
                    if len(res) >= 12:
                        assert(len(res) < 14)
                       
                        if len(res) == 12:
                            f = fix(s1, s2, 0, d2, 0, m, res)
                            if f:
                                return f
                        else:
                            for a in res:
                                f = fix(s1, s2, 0, d2, 0, m, list(filter(lambda x: x is not a, res)))
                                if f:
                                    return f
    return None

# Older, slower way to find matches.
# Needs indexes built, enable in scanner constructor.
def findol(s1, s2):
    for d1 in range(1):
        for d2 in range(3):
                m1 = 0
                for m2 in range(2):
                    for ds1 in s1.diffs[d1][m1]:            
                        for ds2 in s2.diffs[d2][m2]:                                        
                            ol = overlap(ds1, ds2)                    
                            if len(ol) >= 12:
                                f = fix(s1, s2, d1, d2, m1, m2, ol)
                                if f:
                                    return f
    return None

def run():

    scanners = []
    num = 0
    beacons = None

    for l in init():
        if l.startswith('--'):
            if beacons:
                scanners.append(Scanner(num, beacons))
            beacons = []
            num = int(l.split('scanner ')[1].split(' ')[0])
            #if num > 1:
            #    break
            continue
        if len(l) == 0:
            continue
        bpos = map(int, l.split(','))
        beacons.append(Pos(*bpos))

    if beacons:
        scanners.append(Scanner(num, beacons))

    fixed = [scanners[0]]
    remaining = scanners[1:]
    while len(remaining):
        for s in fixed:
            newrem = []
            for s2 in remaining:
                ol = altfind(s, s2)
#                ol = findol(s, s2)
                if ol:
                    fixed.append(ol)
                else:
                    newrem.append(s2)
            remaining = newrem

    beacons = set()
    for s in fixed:
        for b in s.beacons:
            beacons.add(b)

    print(len(beacons))
    maxdist = 0
    for ix, s1 in enumerate(fixed):
        for s2 in fixed[ix+1:]:
            dist = 0
            for d in range(3):
                dist += abs(s1.deltas[d]-s2.deltas[d])
            if maxdist < dist:
                maxdist = dist
    print(maxdist)
run()
