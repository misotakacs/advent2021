#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

static uint64_t divs[14] = {1,  1,   1,  1,  26,  1, 26,  26,  1,  1, 26,  26,  26, 26};
static uint64_t add1[14] = {14, 13, 13, 12, -12, 12, -2, -11, 13, 14,  0, -12, -13, -6};
static uint64_t add2[14] = {8,  8,   3, 10,   8,  8,  8,   5,  9,  3,  4,   9,   2,  7};

uint64_t part(char c, uint64_t z, uint64_t d, uint64_t a, uint64_t a2) {
    uint64_t x = z;
    z = z / d;   // z < 26
    if (c != (x%26) + a) {
        z = z*26 + c + a2;
    }
//    printf("z=%ld\n", z);
    return z;
}

uint64_t run(char c[14]) {
    uint64_t z = 0;
    for(int i = 0; i < 14; i++) {
        printf("%ld\n", z);
        z = part(c[i], z, divs[i], add1[i], add2[i]);
    }
    return z;
}

int gen(int ix, uint64_t z) {
    if (ix == 14) {
        return z;
    }
    if (ix == 13) {
        if (z != 0 && (z < 7 || z > 15)) {
            return -1;
        }
    }
    if (ix == 12) {
        if (z > 16*26) {
            return -1;
        }
    }
    if (ix == 11) {
        if (z > 16*26*26) {
            return -1;
        }
    }
    if (ix == 10) {
        if (z > 16*26*26*26) {
            return -1;
        }
    }    
    // Change this to reverse direction.
    for (char c = 9; c > 0; c--) {
    //for (char c = 1; c < 10; c++) {
        uint64_t nz = part(c, z, divs[ix], add1[ix], add2[ix]);
        uint64_t res = gen(ix+1, nz);
        if (res == 0) {
            printf("%c", c+'0');
            return 0;
        }
    }

    return -1;
}

int main(int argc, char **argv) {

    if (argc > 1) {
        // Testing - pass in a number to run it through the algo.
        char * n = argv[1];
        char num[14];
        for(int i = 0; i < 14; i++) {
            num[i] = n[i] - '0';
        }
        printf("z(%s)=%ld\n", argv[1], run(num));
        exit(0);
    }

    gen(0, 0);
    printf("\n");
}
