import sys
import collections
import re

def getlines():
    ip = []
    for line in sys.stdin:
            ip.append(line.strip())

    return ip

INP = 0
ADD = 1
MUL = 2
DIV = 3
MOD = 4
EQL = 5

RE = r'(inp|add|mul|div|mod|eql) ([wxyz]).?([-wxyz0-9]+)?'

def exec(ins, inp):
    var = {
        'x': 0,
        'y': 0,
        'z': 0,
        'w': 0,
    }
    for i in ins:        
        op = i[0]
        if op == 'inp':
            print(var['z'])        
            var[i[1]] = inp.pop(0)
        elif op == 'add':
            if i[2] == INT:
                var[i[1]] += i[3]
            else:
                var[i[1]] += var[i[3]]
        elif op == 'mul':
            if i[2] == INT:
                var[i[1]] *= i[3]
            else:
                var[i[1]] *= var[i[3]]
        elif op == 'div':
            if i[2] == INT:
                var[i[1]] = var[i[1]] // i[3]
            else:
                var[i[1]] = var[i[1]] // var[i[3]]
        elif op == 'mod':
            if i[2] == INT:
                var[i[1]] = var[i[1]] % i[3]
            else:
                var[i[1]] = var[i[1]] % var[i[3]]
        elif op == 'eql':
            if i[2] == INT:
                var[i[1]] = 1 if var[i[1]] == i[3] else 0
            else:
                var[i[1]] = 1 if var[i[1]] == var[i[3]] else 0

    return var['z']

INT = 0
VAR = 1

def run():
    ls = getlines()

    ins = []
    for l in ls:
        m = re.match(RE, l)
        gs = list(m.groups())
        if gs[2] is not None:
            gs.append(gs[2])
            if gs[2] not in 'wxyz':
                gs[2] = INT
                gs[3] = int(gs[3])
            else:
                gs[2] = VAR

        ins.append(gs)

#    start = 99999999999999
#
#    res = 1
#    it = 0
#    while res:
#        it += 1
#        if it % 1000 == 0:
#            print(it)
#        res = exec(ins, [int(c) for c in str(start)])
#        print(start, res)
#        start -= 1


    if len(sys.argv) > 1:
        print('z(%s)=' % sys.argv[1])
        print(exec(ins, [int(c) for c in sys.argv[1]]))
    else:
        vs = ['9' * 14]
        vs = ['13579246899999']

        for v in vs:
            print(exec(ins, [int(c) for c in v]))

run()
